package com.brick4u.service.custom.entities;

import org.influxdb.annotation.Column;
import org.influxdb.annotation.Measurement;

@Measurement(name="MDT-A511-71080642-03")
public class Entity71080642 {
    @Column(name="time")
    private String time;

    @Column(name = "FLOW_TEMPERATURE")
    private Double FLOW_TEMPERATURE;

    @Column(name = "VOLUME")
    private Double VOLUME;

    @Column(name="VOLUME_FLOW")
    private Double VOLUME_FLOW;

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public Double getFLOW_TEMPERATURE() {
        return FLOW_TEMPERATURE;
    }

    public void setFLOW_TEMPERATURE(Double FLOW_TEMPERATURE) {
        this.FLOW_TEMPERATURE = FLOW_TEMPERATURE;
    }

    public Double getVOLUME() {
        return VOLUME;
    }

    public void setVOLUME(Double VOLUME) {
        this.VOLUME = VOLUME;
    }

    public Double getVOLUME_FLOW() {
        return VOLUME_FLOW;
    }

    public void setVOLUME_FLOW(Double VOLUME_FLOW) {
        this.VOLUME_FLOW = VOLUME_FLOW;
    }
}
