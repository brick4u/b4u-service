package com.brick4u.service.custom.entities;

import org.influxdb.annotation.Column;
import org.influxdb.annotation.Measurement;

@Measurement(name="MDT-B409-03920817-07")
public class Entity03920817 {

    @Column(name="time")
    private String   time;

    @Column(name="AVERAGING_DURATION")
    private Double   AVERAGING_DURATION;

    @Column(name = "DATE_TIME")
    private Double  DATE_TIME;

    @Column(name="VOLUME")
    private Double VOLUME;

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public Double getAVERAGING_DURATION() {
        return AVERAGING_DURATION;
    }

    public void setAVERAGING_DURATION(Double AVERAGING_DURATION) {
        this.AVERAGING_DURATION = AVERAGING_DURATION;
    }

    public Double getDATE_TIME() {
        return DATE_TIME;
    }

    public void setDATE_TIME(Double DATE_TIME) {
        this.DATE_TIME = DATE_TIME;
    }

    public Double getVOLUME() {
        return VOLUME;
    }

    public void setVOLUME(Double VOLUME) {
        this.VOLUME = VOLUME;
    }
}
