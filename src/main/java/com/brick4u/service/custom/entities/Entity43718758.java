package com.brick4u.service.custom.entities;

import org.influxdb.annotation.Column;
import org.influxdb.annotation.Measurement;

@Measurement(name="MDT-A511-43718758-04")
public class Entity43718758 {

    @Column(name = "time")
    private String time;

    @Column(name="CUSTOMER")
    private Double CUSTOMER;

    @Column(name="VOLUME")
    private Double VOLUME;

    public Double getVOLUME() {
        return VOLUME;
    }

    public void setVOLUME(Double VOLUME) {
        this.VOLUME = VOLUME;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public Double getCUSTOMER() {
        return CUSTOMER;
    }

    public void setCUSTOMER(Double CUSTOMER) {
        this.CUSTOMER = CUSTOMER;
    }
}
