package com.brick4u.service.custom.entities;

import org.influxdb.annotation.Column;
import org.influxdb.annotation.Measurement;

@Measurement(name="MDT-0106-15050080-16")
public class Entity15050080 {

    @Column(name="time")
    private String time;

    @Column(name="ERROR_FLAGS")
    private Double ERROR_FLAGS;

    @Column(name="VOLUME")
    private Double VOLUME;

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public Double getERROR_FLAGS() {
        return ERROR_FLAGS;
    }

    public void setERROR_FLAGS(Double ERROR_FLAGS) {
        this.ERROR_FLAGS = ERROR_FLAGS;
    }

    public Double getVOLUME() {
        return VOLUME;
    }

    public void setVOLUME(Double VOLUME) {
        this.VOLUME = VOLUME;
    }
}
