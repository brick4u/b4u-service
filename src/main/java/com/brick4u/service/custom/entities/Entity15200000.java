package com.brick4u.service.custom.entities;

import org.checkerframework.checker.units.qual.C;
import org.influxdb.annotation.Column;
import org.influxdb.annotation.Measurement;

@Measurement(name="MDT-5A14-15200000-37")
public class Entity15200000 {
    @Column(name="time")
    private String time;

    @Column(name="CUSTOMER")
    private Integer CUSTOMER;

    @Column(name="CUSTOMER_LOCATION")
    private Integer CUSTOMER_LOCATION;

    @Column(name="ENERGY")
    private Double ENERGY;

    @Column(name="ENERGY_1ENERGY_2")
    private Double ENERGY_1ENERGY_2;

    @Column(name="ENERGY_3")
    private Double ENERGY_3;

    @Column(name="NOT_SUPPORTED")
    private Double NOT_SUPPORTED;

    @Column(name="OTHER_SOFTWARE_VERSION")
    private Double OTHER_SOFTWARE_VERSION;

    @Column(name="POWER")
    private Double POWER;

    @Column(name="POWER_2")
    private Double POWER_2;

    @Column(name="POWER_3")
    private Double POWER_3;

    @Column(name="POWER_5")
    private Double POWER_5;

    @Column(name="POWER_6")
    private Double POWER_6;

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public Integer getCUSTOMER() {
        return CUSTOMER;
    }

    public void setCUSTOMER(Integer CUSTOMER) {
        this.CUSTOMER = CUSTOMER;
    }

    public Integer getCUSTOMER_LOCATION() {
        return CUSTOMER_LOCATION;
    }

    public void setCUSTOMER_LOCATION(Integer CUSTOMER_LOCATION) {
        this.CUSTOMER_LOCATION = CUSTOMER_LOCATION;
    }

    public Double getENERGY() {
        return ENERGY;
    }

    public void setENERGY(Double ENERGY) {
        this.ENERGY = ENERGY;
    }

    public Double getENERGY_1ENERGY_2() {
        return ENERGY_1ENERGY_2;
    }

    public void setENERGY_1ENERGY_2(Double ENERGY_1ENERGY_2) {
        this.ENERGY_1ENERGY_2 = ENERGY_1ENERGY_2;
    }

    public Double getENERGY_3() {
        return ENERGY_3;
    }

    public void setENERGY_3(Double ENERGY_3) {
        this.ENERGY_3 = ENERGY_3;
    }

    public Double getNOT_SUPPORTED() {
        return NOT_SUPPORTED;
    }

    public void setNOT_SUPPORTED(Double NOT_SUPPORTED) {
        this.NOT_SUPPORTED = NOT_SUPPORTED;
    }

    public Double getOTHER_SOFTWARE_VERSION() {
        return OTHER_SOFTWARE_VERSION;
    }

    public void setOTHER_SOFTWARE_VERSION(Double OTHER_SOFTWARE_VERSION) {
        this.OTHER_SOFTWARE_VERSION = OTHER_SOFTWARE_VERSION;
    }

    public Double getPOWER() {
        return POWER;
    }

    public void setPOWER(Double POWER) {
        this.POWER = POWER;
    }

    public Double getPOWER_2() {
        return POWER_2;
    }

    public void setPOWER_2(Double POWER_2) {
        this.POWER_2 = POWER_2;
    }

    public Double getPOWER_3() {
        return POWER_3;
    }

    public void setPOWER_3(Double POWER_3) {
        this.POWER_3 = POWER_3;
    }

    public Double getPOWER_5() {
        return POWER_5;
    }

    public void setPOWER_5(Double POWER_5) {
        this.POWER_5 = POWER_5;
    }

    public Double getPOWER_6() {
        return POWER_6;
    }

    public void setPOWER_6(Double POWER_6) {
        this.POWER_6 = POWER_6;
    }
}
