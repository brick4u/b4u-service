/**
 * View Models used by Spring MVC REST controllers.
 */
package com.brick4u.service.web.rest.vm;
