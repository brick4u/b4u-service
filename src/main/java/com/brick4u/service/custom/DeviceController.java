package com.brick4u.service.custom;

import com.brick4u.service.custom.dtos.DeviceDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class DeviceController {


    private static final Logger log = LoggerFactory.getLogger(DeviceController.class);

    @Autowired()
    private DeviceService deviceService;

    @GetMapping("/hersteller/{herstellerId}/sparte/{sparteId}/geraet/{geraetId}")
    public DeviceDto getDeviceDataById(@PathVariable String herstellerId, @PathVariable String sparteId, @PathVariable String geraetId) {
        try {
            return deviceService.getDeviceData(herstellerId, geraetId, sparteId);
        } catch (Exception exception) {
            log.error(exception.getLocalizedMessage());
            return null;
        }
    }
}
