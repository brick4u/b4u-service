package com.brick4u.service.custom.entities;

import org.influxdb.annotation.Column;
import org.influxdb.annotation.Measurement;

@Measurement(name="MDT-B05C-23000000-1B")
public class Entity23000000 {

    @Column(name="time")
    private String time;

    @Column(name="ERROR_FLAGS")
    private double ERROR_FLAGS;

    @Column(name="EXTERNAL_TEMPERATURE")
    private double EXTERNAL_TEMPERATURE;

    @Column(name="REL_HUMIDITY")
    private Double REL_HUMIDITY;

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public double getERROR_FLAGS() {
        return ERROR_FLAGS;
    }

    public void setERROR_FLAGS(double ERROR_FLAGS) {
        this.ERROR_FLAGS = ERROR_FLAGS;
    }

    public double getEXTERNAL_TEMPERATURE() {
        return EXTERNAL_TEMPERATURE;
    }

    public void setEXTERNAL_TEMPERATURE(double EXTERNAL_TEMPERATURE) {
        this.EXTERNAL_TEMPERATURE = EXTERNAL_TEMPERATURE;
    }

    public Double getREL_HUMIDITY() {
        return REL_HUMIDITY;
    }

    public void setREL_HUMIDITY(Double REL_HUMIDITY) {
        this.REL_HUMIDITY = REL_HUMIDITY;
    }
}
