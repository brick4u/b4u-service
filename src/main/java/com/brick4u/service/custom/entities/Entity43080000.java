package com.brick4u.service.custom.entities;

import org.influxdb.annotation.Column;
import org.influxdb.annotation.Measurement;

@Measurement(name="MDT-5A14-43080000-02")
public class Entity43080000 {

    @Column(name="time")
    private String time;

    @Column(name="CUSTOMER")
    private Double CUSTOMER;

    @Column(name="CUSTOMER_LOCATION")
    private Double CUSTOMER_LOCATION;

    @Column(name="ENERGY")
    private Double ENERGY;

    @Column(name="FABRICATION_NO")
    private Double FABRICATION_NO;

    @Column(name = "NOT_SUPPORTED")
    private Double NOT_SUPPORTED;

    @Column(name="OTHER_SOFTWARE_VERSION")
    private Double OTHER_SOFTWARE_VERSION;

    @Column(name="POWER")
    private Double	POWER;

    @Column(name = "POWER_2")
    private Double    POWER_2;

    @Column(name = "POWER_3")
    private Double    POWER_3;

    @Column(name="VOLTAGE")
    private Double	VOLTAGE;

    @Column(name = "VOLTAGE_5")
    private Double    VOLTAGE_5;

    @Column(name = "VOLTAGE_6")
    private Double    VOLTAGE_6;


    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public Double getCUSTOMER() {
        return CUSTOMER;
    }

    public void setCUSTOMER(Double CUSTOMER) {
        this.CUSTOMER = CUSTOMER;
    }

    public Double getCUSTOMER_LOCATION() {
        return CUSTOMER_LOCATION;
    }

    public void setCUSTOMER_LOCATION(Double CUSTOMER_LOCATION) {
        this.CUSTOMER_LOCATION = CUSTOMER_LOCATION;
    }

    public Double getENERGY() {
        return ENERGY;
    }

    public void setENERGY(Double ENERGY) {
        this.ENERGY = ENERGY;
    }

    public Double getFABRICATION_NO() {
        return FABRICATION_NO;
    }

    public void setFABRICATION_NO(Double FABRICATION_NO) {
        this.FABRICATION_NO = FABRICATION_NO;
    }

    public Double getNOT_SUPPORTED() {
        return NOT_SUPPORTED;
    }

    public void setNOT_SUPPORTED(Double NOT_SUPPORTED) {
        this.NOT_SUPPORTED = NOT_SUPPORTED;
    }

    public Double getOTHER_SOFTWARE_VERSION() {
        return OTHER_SOFTWARE_VERSION;
    }

    public void setOTHER_SOFTWARE_VERSION(Double OTHER_SOFTWARE_VERSION) {
        this.OTHER_SOFTWARE_VERSION = OTHER_SOFTWARE_VERSION;
    }

    public Double getPOWER() {
        return POWER;
    }

    public void setPOWER(Double POWER) {
        this.POWER = POWER;
    }

    public Double getPOWER_2() {
        return POWER_2;
    }

    public void setPOWER_2(Double POWER_2) {
        this.POWER_2 = POWER_2;
    }

    public Double getPOWER_3() {
        return POWER_3;
    }

    public void setPOWER_3(Double POWER_3) {
        this.POWER_3 = POWER_3;
    }

    public Double getVOLTAGE() {
        return VOLTAGE;
    }

    public void setVOLTAGE(Double VOLTAGE) {
        this.VOLTAGE = VOLTAGE;
    }

    public Double getVOLTAGE_5() {
        return VOLTAGE_5;
    }

    public void setVOLTAGE_5(Double VOLTAGE_5) {
        this.VOLTAGE_5 = VOLTAGE_5;
    }

    public Double getVOLTAGE_6() {
        return VOLTAGE_6;
    }

    public void setVOLTAGE_6(Double VOLTAGE_6) {
        this.VOLTAGE_6 = VOLTAGE_6;
    }
}
