package com.brick4u.service.custom;


import org.influxdb.dto.Query;
import org.influxdb.dto.QueryResult;
import org.influxdb.impl.InfluxDBResultMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.influxdb.InfluxDBConnectionFactory;
import org.springframework.data.influxdb.InfluxDBProperties;
import org.springframework.data.influxdb.InfluxDBTemplate;
import org.springframework.data.influxdb.converter.PointCollectionConverter;

@Configuration
public class InfluxClient {

    private static final Logger log = LoggerFactory.getLogger(InfluxClient.class);

    @Autowired
    private EntityMapper entityMapper;

    /*Influx configuration*/
    private String url = "http://meterbrick.de:8086";
    private String username = "b4uservice";
    private String password = "8k3orEsXKLQkh2DLTJnjv3ZHn9Dqp";
    private String databasename = "wmbus";
    private InfluxDBTemplate influxDBTemplate;
    private InfluxDBProperties influxDBProperties = new InfluxDBProperties();
    private InfluxDBConnectionFactory influxDBConnectionFactory = new InfluxDBConnectionFactory();
    private PointCollectionConverter pointCollectionConverter;

    public InfluxClient() {
        influxDBProperties.setUrl(url);
        influxDBProperties.setUsername(username);
        influxDBProperties.setRetentionPolicy("autogen");
        influxDBProperties.setPassword(password);
        influxDBConnectionFactory.setProperties(influxDBProperties);
        influxDBTemplate = new InfluxDBTemplate(influxDBConnectionFactory, pointCollectionConverter);
        log.info("Database connection success!");
    }

    public QueryResult getQueryResult(String measurement) {
        try {
            QueryResult queryResult = influxDBTemplate.query(new Query("select * from \"" + measurement + "\" limit 1", databasename));
            InfluxDBResultMapper resultMapper = new InfluxDBResultMapper();
            return queryResult;
        } catch (Exception exception) {
            log.error(exception.getLocalizedMessage());
            return null;
        }
    }
}
