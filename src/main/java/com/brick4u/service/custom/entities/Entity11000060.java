package com.brick4u.service.custom.entities;

import org.influxdb.annotation.Column;
import org.influxdb.annotation.Measurement;

@Measurement(name="MDT-7916-11000060-02")
public class Entity11000060 {

    @Column(name="time")
    private String time;

    @Column(name="ENERGY")
    private Double ENERGY;

    @Column(name="EXTENDED_IDENTIFICATION")
    private Double EXTENDED_IDENTIFICATION;

    @Column(name="EXTERNAL_TEMPERATURE")
    private Double EXTERNAL_TEMPERATURE;

    @Column(name = "MASS")
    private Double MASS;



    @Column(name="FABRICATION_NO")
    private Double FABRICATION_NO;

    public Double getNOT_SUPPORTED() {
        return NOT_SUPPORTED;
    }

    public void setNOT_SUPPORTED(Double NOT_SUPPORTED) {
        this.NOT_SUPPORTED = NOT_SUPPORTED;
    }

    @Column(name="NOT_SUPPORTED")
    private Double NOT_SUPPORTED;

    @Column(name="POWER")
    private Double	POWER;

    @Column(name = "POWER_1")
    private Double    POWER_1;

    @Column(name = "POWER_2")
    private Double    POWER_2;

    @Column(name = "POWER_3")
    private Double    POWER_3;

    @Column(name = "POWER_4")
    private Double    POWER_4;

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public Double getENERGY() {
        return ENERGY;
    }

    public void setENERGY(Double ENERGY) {
        this.ENERGY = ENERGY;
    }

    public Double getEXTENDED_IDENTIFICATION() {
        return EXTENDED_IDENTIFICATION;
    }

    public void setEXTENDED_IDENTIFICATION(Double EXTENDED_IDENTIFICATION) {
        this.EXTENDED_IDENTIFICATION = EXTENDED_IDENTIFICATION;
    }

    public Double getFABRICATION_NO() {
        return FABRICATION_NO;
    }

    public void setFABRICATION_NO(Double FABRICATION_NO) {
        this.FABRICATION_NO = FABRICATION_NO;
    }

    public Double getPOWER() {
        return POWER;
    }

    public void setPOWER(Double POWER) {
        this.POWER = POWER;
    }

    public Double getPOWER_1() {
        return POWER_1;
    }

    public void setPOWER_1(Double POWER_1) {
        this.POWER_1 = POWER_1;
    }

    public Double getPOWER_2() {
        return POWER_2;
    }

    public void setPOWER_2(Double POWER_2) {
        this.POWER_2 = POWER_2;
    }

    public Double getPOWER_3() {
        return POWER_3;
    }

    public void setPOWER_3(Double POWER_3) {
        this.POWER_3 = POWER_3;
    }

    public Double getPOWER_4() {
        return POWER_4;
    }

    public void setPOWER_4(Double POWER_4) {
        this.POWER_4 = POWER_4;
    }
    public Double getEXTERNAL_TEMPERATURE() {
        return EXTERNAL_TEMPERATURE;
    }

    public void setEXTERNAL_TEMPERATURE(Double EXTERNAL_TEMPERATURE) {
        this.EXTERNAL_TEMPERATURE = EXTERNAL_TEMPERATURE;
    }

    public Double getMASS() {
        return MASS;
    }

    public void setMASS(Double MASS) {
        this.MASS = MASS;
    }

}
