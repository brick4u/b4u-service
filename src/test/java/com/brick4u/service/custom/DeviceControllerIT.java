package com.brick4u.service.custom;

import com.brick4u.service.JhipsterApp;
import com.brick4u.service.web.rest.errors.ExceptionTranslator;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.validation.Validator;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static com.brick4u.service.web.rest.TestUtil.createFormattingConversionService;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest(classes = JhipsterApp.class)
@AutoConfigureMockMvc
public class DeviceControllerIT {

    private static final Logger log = LoggerFactory.getLogger(DeviceControllerIT.class);


    @Autowired
    private MockMvc mockMvc;

    @Autowired
    DeviceController deviceController;

    @Autowired
    DeviceService deviceService;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private Validator validator;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final DeviceController deviceController = new DeviceController();
        this.mockMvc = MockMvcBuilders.standaloneSetup(deviceController)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    @Test
    void DeviceDataUT() {
        String HerstellerId = "A815";
        String SparteId = "02";
        String GeraetId = "07922213";
        String result = null;
        try {
            result = mockMvc.perform(get("/api/hersteller/" + HerstellerId + "/sparte/" + SparteId + "/geraet/" + GeraetId))
                .andExpect(status().isOk())
                .andReturn()
                .getResponse()
                .getContentAsString();
            log.info(result);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
