package com.brick4u.service.custom.entities;

import org.influxdb.annotation.Column;
import org.influxdb.annotation.Measurement;

@Measurement(name = "MDT-A511-03018082-37")
public class Entity03018082 {

    @Column(name="time")
    private String time;

    @Column(name = "FIRMWARE_VERSION")
    private Double FIRMWARE_VERSION;

    @Column(name = "OPERATING_TIME")
    private Double OPERATING_TIME;

    @Column(name = "REMAINING_BATTERY_LIFE_TIME")
    private Double REMAINING_BATTERY_LIFE_TIME;

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public Double getFIRMWARE_VERSION() {
        return FIRMWARE_VERSION;
    }

    public void setFIRMWARE_VERSION(Double FIRMWARE_VERSION) {
        this.FIRMWARE_VERSION = FIRMWARE_VERSION;
    }

    public Double getOPERATING_TIME() {
        return OPERATING_TIME;
    }

    public void setOPERATING_TIME(Double OPERATING_TIME) {
        this.OPERATING_TIME = OPERATING_TIME;
    }

    public Double getREMAINING_BATTERY_LIFE_TIME() {
        return REMAINING_BATTERY_LIFE_TIME;
    }

    public void setREMAINING_BATTERY_LIFE_TIME(Double REMAINING_BATTERY_LIFE_TIME) {
        this.REMAINING_BATTERY_LIFE_TIME = REMAINING_BATTERY_LIFE_TIME;
    }
}
