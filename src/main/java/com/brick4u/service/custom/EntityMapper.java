package com.brick4u.service.custom;


import com.brick4u.service.custom.entities.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Configuration;

import java.util.HashMap;

@Configuration
public class EntityMapper {

    private static final Logger log = LoggerFactory.getLogger(EntityMapper.class);

    HashMap<String, Class> hmap = new HashMap<String, Class>();
    public void AddMapping(String geraetId,Class EntityClass)
    {
        hmap.put(geraetId,EntityClass);
    }

    public EntityMapper() {
        hmap.put("03018082",Entity03018082.class);
        hmap.put("03920817",Entity03920817.class);
        hmap.put("11000060",Entity11000060.class);
        hmap.put("13229207",Entity13229207.class);
        hmap.put("14229207",Entity14229207.class);
        hmap.put("15050080",Entity15050080.class);
        hmap.put("15200000",Entity15200000.class);
        hmap.put("16229207",Entity16229207.class);
        hmap.put("17229207",Entity17229207.class);
        hmap.put("18229207",Entity18229207.class);
        hmap.put("19229207",Entity19229207.class);
        hmap.put("20229207",Entity20229207.class);
        hmap.put("21229207",Entity21229207.class);
        hmap.put("21580008",Entity21580008.class);
        hmap.put("22229207",Entity22229207.class);
        hmap.put("23000000",Entity23000000.class);
        hmap.put("23229207",Entity23229207.class);
        hmap.put("24396500",Entity24396500.class);
        hmap.put("24545051",Entity24545051.class);
        hmap.put("27229207",Entity27229207.class);
        hmap.put("31320008",Entity31320008.class);
        hmap.put("36320008",Entity36320008.class);
        hmap.put("37489063",Entity37489063.class);
        hmap.put("37710000",Entity37710000.class);
        hmap.put("43080000",Entity43080000.class);
        hmap.put("43718758",Entity43718758.class);
        hmap.put("44246555",Entity44246555.class);
        hmap.put("44570608",Entity44570608.class);
        hmap.put("45980661",Entity45980661.class);
        hmap.put("71080642",Entity71080642.class);
        log.info("Influx mapper initialized successfully!");
    }

    public Class GetInfluxEntity(String HerstellerId)
    {
      return hmap.get(HerstellerId);
    }
}
