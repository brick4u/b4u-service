package com.brick4u.service.custom;


import com.brick4u.service.custom.dtos.DeviceDto;
import org.influxdb.dto.QueryResult;
import org.influxdb.impl.InfluxDBResultMapper;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DeviceService {

    ModelMapper modelMapper = new ModelMapper();
    private static final Logger log = LoggerFactory.getLogger(DeviceService.class);
    @Autowired
    private InfluxClient influxClient;
    @Autowired
    private EntityMapper entityMapper;

    public DeviceDto getDeviceData(String herstellerId, String geraetId, String sparteId) {
        DeviceDto deviceDto;
        try {
            geraetId=getHerstellerId(geraetId);
            QueryResult queryResult = influxClient.getQueryResult("MDT-" + herstellerId + "-" + geraetId + "-" + sparteId);
            InfluxDBResultMapper resultMapper = new InfluxDBResultMapper();
            List<Class> result = resultMapper
                .toPOJO(queryResult, entityMapper.GetInfluxEntity(geraetId));
            deviceDto = modelMapper.map(result.get(0), DeviceDto.class);
            return deviceDto;
        } catch (Exception exception) {
            log.error(exception.getMessage());
            return null;
        }
    }

    private String getHerstellerId(String geraetId)
    {
        String substr0 = "";
        String substr1 = "";
        String substr2 = "";
        String substr3 = "";

        substr3 = geraetId.substring(0, 2);
        substr2 = geraetId.substring(2,4);
        substr1 = geraetId.substring(4,6);
        substr0 = geraetId.substring(6,8);
        return substr0+substr1+substr2+substr3;
    }
}
