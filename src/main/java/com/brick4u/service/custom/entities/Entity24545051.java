package com.brick4u.service.custom.entities;

import org.influxdb.annotation.Column;
import org.influxdb.annotation.Measurement;

@Measurement(name = "MDT-2423-24545051-07")
public class Entity24545051 {

    @Column(name = "time")
    private String time;

    @Column(name = "REMAINING_BATTERY_LIFE_TIME")
    private int REMAINING_BATTERY_LIFE_TIME;

    @Column(name = "VOLUME")
    private double VOLUME;

    @Column(name = "VOLUME_FLOW")
    private double VOLUME_FLOW;

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public int getREMAINING_BATTERY_LIFE_TIME() {
        return REMAINING_BATTERY_LIFE_TIME;
    }

    public void setREMAINING_BATTERY_LIFE_TIME(int REMAINING_BATTERY_LIFE_TIME) {
        this.REMAINING_BATTERY_LIFE_TIME = REMAINING_BATTERY_LIFE_TIME;
    }

    public double getVOLUME() {
        return VOLUME;
    }

    public void setVOLUME(double VOLUME) {
        this.VOLUME = VOLUME;
    }

    public double getVOLUME_FLOW() {
        return VOLUME_FLOW;
    }

    public void setVOLUME_FLOW(double VOLUME_FLOW) {
        this.VOLUME_FLOW = VOLUME_FLOW;
    }
}
