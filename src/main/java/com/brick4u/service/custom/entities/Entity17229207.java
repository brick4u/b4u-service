package com.brick4u.service.custom.entities;

import org.influxdb.annotation.Column;
import org.influxdb.annotation.Measurement;

@Measurement(name="MDT-A815-17229207-02")
public class Entity17229207 {
    @Column(name="time")
    private String time;

    @Column(name="1.0.1.8.0.255")
    private Double First_Column;

    @Column(name="1.0.1.8.1.255")
    private Double Second_Column;

    @Column(name="1.0.1.8.2.255")
    private Double Third_Column;

    @Column(name="1.0.16.7.0.255")
    private Double Fourth_Column;

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public Double getFirst_Column() {
        return First_Column;
    }

    public void setFirst_Column(Double first_Column) {
        First_Column = first_Column;
    }

    public Double getSecond_Column() {
        return Second_Column;
    }

    public void setSecond_Column(Double second_Column) {
        Second_Column = second_Column;
    }

    public Double getThird_Column() {
        return Third_Column;
    }

    public void setThird_Column(Double third_Column) {
        Third_Column = third_Column;
    }

    public Double getFourth_Column() {
        return Fourth_Column;
    }

    public void setFourth_Column(Double fourth_Column) {
        Fourth_Column = fourth_Column;
    }
}
