package com.brick4u.service.custom.entities;

import org.bouncycastle.cms.PasswordRecipientId;
import org.influxdb.annotation.Column;
import org.influxdb.annotation.Measurement;

@Measurement(name="MDT-A511-44246555-04")
public class Entity44246555 {

    @Column(name="time")
    private String time;

    @Column(name="ENERGY")
    private Double ENERGY;

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public Double getENERGY() {
        return ENERGY;
    }

    public void setENERGY(Double ENERGY) {
        this.ENERGY = ENERGY;
    }
}
