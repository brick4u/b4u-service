package com.brick4u.service.custom.dtos;

import org.influxdb.annotation.Column;

public class DeviceDto {

    private String time;

    private Double First_Column;

    private Double Second_Column;

    private Double Third_Column;

    private Double Fourth_Column;

    private Double Fifth_Column;

    private Double Sixth_Column;

    private Double Seventh_Column;

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public Double getFirst_Column() {
        return First_Column;
    }

    public void setFirst_Column(Double first_Column) {
        First_Column = first_Column;
    }

    public Double getSecond_Column() {
        return Second_Column;
    }

    public void setSecond_Column(Double second_Column) {
        Second_Column = second_Column;
    }

    public Double getThird_Column() {
        return Third_Column;
    }

    public void setThird_Column(Double third_Column) {
        Third_Column = third_Column;
    }

    public Double getFourth_Column() {
        return Fourth_Column;
    }

    public void setFourth_Column(Double fourth_Column) {
        Fourth_Column = fourth_Column;
    }

    public Double getFifth_Column() {
        return Fifth_Column;
    }

    public void setFifth_Column(Double fifth_Column) {
        Fifth_Column = fifth_Column;
    }

    public Double getSixth_Column() {
        return Sixth_Column;
    }

    public void setSixth_Column(Double sixth_Column) {
        Sixth_Column = sixth_Column;
    }

    public Double getSeventh_Column() {
        return Seventh_Column;
    }

    public void setSeventh_Column(Double seventh_Column) {
        Seventh_Column = seventh_Column;
    }

    // for Type 03
    private Double FLOW_TEMPERATURE;

    private Double VOLUME;

    private Double VOLUME_FLOW;

    public Double getFLOW_TEMPERATURE() {
        return FLOW_TEMPERATURE;
    }

    public void setFLOW_TEMPERATURE(Double FLOW_TEMPERATURE) {
        this.FLOW_TEMPERATURE = FLOW_TEMPERATURE;
    }

    public Double getVOLUME() {
        return VOLUME;
    }

    public void setVOLUME(Double VOLUME) {
        this.VOLUME = VOLUME;
    }

    public Double getVOLUME_FLOW() {
        return VOLUME_FLOW;
    }

    public void setVOLUME_FLOW(Double VOLUME_FLOW) {
        this.VOLUME_FLOW = VOLUME_FLOW;
    }

    //for Type 16
    private String ERROR_FLAGS;

    public String getERROR_FLAGS() {
        return ERROR_FLAGS;
    }

    public void setERROR_FLAGS(String ERROR_FLAGS) {
        this.ERROR_FLAGS = ERROR_FLAGS;
    }

    //For Type 37

    private Integer CUSTOMER;

    private Integer CUSTOMER_LOCATION;

    private Double ENERGY;

    private Double ENERGY_1ENERGY_2;

    private Double ENERGY_3;

    private Double NOT_SUPPORTED;

    private Double OTHER_SOFTWARE_VERSION;

    private Double POWER;

    private Double POWER_2;

    private Double POWER_3;

    private Double POWER_5;

    private Double POWER_6;

    public Integer getCUSTOMER() {
        return CUSTOMER;
    }

    public void setCUSTOMER(Integer CUSTOMER) {
        this.CUSTOMER = CUSTOMER;
    }

    public Integer getCUSTOMER_LOCATION() {
        return CUSTOMER_LOCATION;
    }

    public void setCUSTOMER_LOCATION(Integer CUSTOMER_LOCATION) {
        this.CUSTOMER_LOCATION = CUSTOMER_LOCATION;
    }

    public Double getENERGY() {
        return ENERGY;
    }

    public void setENERGY(Double ENERGY) {
        this.ENERGY = ENERGY;
    }

    public Double getENERGY_1ENERGY_2() {
        return ENERGY_1ENERGY_2;
    }

    public void setENERGY_1ENERGY_2(Double ENERGY_1ENERGY_2) {
        this.ENERGY_1ENERGY_2 = ENERGY_1ENERGY_2;
    }

    public Double getENERGY_3() {
        return ENERGY_3;
    }

    public void setENERGY_3(Double ENERGY_3) {
        this.ENERGY_3 = ENERGY_3;
    }

    public Double getNOT_SUPPORTED() {
        return NOT_SUPPORTED;
    }

    public void setNOT_SUPPORTED(Double NOT_SUPPORTED) {
        this.NOT_SUPPORTED = NOT_SUPPORTED;
    }

    public Double getOTHER_SOFTWARE_VERSION() {
        return OTHER_SOFTWARE_VERSION;
    }

    public void setOTHER_SOFTWARE_VERSION(Double OTHER_SOFTWARE_VERSION) {
        this.OTHER_SOFTWARE_VERSION = OTHER_SOFTWARE_VERSION;
    }

    public Double getPOWER() {
        return POWER;
    }

    public void setPOWER(Double POWER) {
        this.POWER = POWER;
    }

    public Double getPOWER_2() {
        return POWER_2;
    }

    public void setPOWER_2(Double POWER_2) {
        this.POWER_2 = POWER_2;
    }

    public Double getPOWER_3() {
        return POWER_3;
    }

    public void setPOWER_3(Double POWER_3) {
        this.POWER_3 = POWER_3;
    }

    public Double getPOWER_5() {
        return POWER_5;
    }

    public void setPOWER_5(Double POWER_5) {
        this.POWER_5 = POWER_5;
    }

    public Double getPOWER_6() {
        return POWER_6;
    }

    public void setPOWER_6(Double POWER_6) {
        this.POWER_6 = POWER_6;
    }

    //Type 03920817


    private Double   AVERAGING_DURATION;


    private Double  DATE_TIME;

    public Double getAVERAGING_DURATION() {
        return AVERAGING_DURATION;
    }

    public void setAVERAGING_DURATION(Double AVERAGING_DURATION) {
        this.AVERAGING_DURATION = AVERAGING_DURATION;
    }

    public Double getDATE_TIME() {
        return DATE_TIME;
    }

    public void setDATE_TIME(Double DATE_TIME) {
        this.DATE_TIME = DATE_TIME;
    }

    // for 11000060

    //private Double ENERGY;

    private Double EXTENDED_IDENTIFICATION;

    private Double EXTERNAL_TEMPERATURE;

    private Double MASS;

    private Double FABRICATION_NO;

//    private Double	POWER;

    private Double    POWER_1;

    public Double getEXTENDED_IDENTIFICATION() {
        return EXTENDED_IDENTIFICATION;
    }

    public void setEXTENDED_IDENTIFICATION(Double EXTENDED_IDENTIFICATION) {
        this.EXTENDED_IDENTIFICATION = EXTENDED_IDENTIFICATION;
    }

    public Double getEXTERNAL_TEMPERATURE() {
        return EXTERNAL_TEMPERATURE;
    }

    public void setEXTERNAL_TEMPERATURE(Double EXTERNAL_TEMPERATURE) {
        this.EXTERNAL_TEMPERATURE = EXTERNAL_TEMPERATURE;
    }

    public Double getMASS() {
        return MASS;
    }

    public void setMASS(Double MASS) {
        this.MASS = MASS;
    }

    public Double getFABRICATION_NO() {
        return FABRICATION_NO;
    }

    public void setFABRICATION_NO(Double FABRICATION_NO) {
        this.FABRICATION_NO = FABRICATION_NO;
    }

    public Double getPOWER_1() {
        return POWER_1;
    }

    public void setPOWER_1(Double POWER_1) {
        this.POWER_1 = POWER_1;
    }

    public Double getPOWER_4() {
        return POWER_4;
    }

    public void setPOWER_4(Double POWER_4) {
        this.POWER_4 = POWER_4;
    }
//    private Double    POWER_2;

//    private Double    POWER_3;

    private Double    POWER_4;

    //24396500

   /* @Column(name ="CUSTOMER")
    private Double CUSTOMER;

    @Column(name = "CUSTOMER_LOCATION")
    private Double	CUSTOMER_LOCATION;


    @Column(name="ENERGY")
    private Double ENERGY;

    @Column(name="FABRICATION_NO")
    private Double FABRICATION_NO;

    @Column(name = "NOT_SUPPORTED")
    private Double NOT_SUPPORTED;

    @Column(name="OTHER_SOFTWARE_VERSION")
    private Double OTHER_SOFTWARE_VERSION;

    @Column(name="POWER")
    private Double	POWER;

    @Column(name = "POWER_2")
    private Double    POWER_2;

    @Column(name = "POWER_3")
    private Double    POWER_3;*/

    //43080000
   /* @Column(name="time")
    private String time;

    @Column(name="CUSTOMER")
    private Double CUSTOMER;

    @Column(name="CUSTOMER_LOCATION")
    private Double CUSTOMER_LOCATION;

    @Column(name="ENERGY")
    private Double ENERGY;

    @Column(name="FABRICATION_NO")
    private Double FABRICATION_NO;

    @Column(name = "NOT_SUPPORTED")
    private Double NOT_SUPPORTED;

    @Column(name="OTHER_SOFTWARE_VERSION")
    private Double OTHER_SOFTWARE_VERSION;

    @Column(name="POWER")
    private Double	POWER;

    @Column(name = "POWER_2")
    private Double    POWER_2;

    @Column(name = "POWER_3")
    private Double    POWER_3;*/

    private Double	VOLTAGE;

    private Double    VOLTAGE_5;

    private Double    VOLTAGE_6;

    public Double getVOLTAGE() {
        return VOLTAGE;
    }

    public void setVOLTAGE(Double VOLTAGE) {
        this.VOLTAGE = VOLTAGE;
    }

    public Double getVOLTAGE_5() {
        return VOLTAGE_5;
    }

    public void setVOLTAGE_5(Double VOLTAGE_5) {
        this.VOLTAGE_5 = VOLTAGE_5;
    }

    public Double getVOLTAGE_6() {
        return VOLTAGE_6;
    }

    public void setVOLTAGE_6(Double VOLTAGE_6) {
        this.VOLTAGE_6 = VOLTAGE_6;
    }

    //03018082
    private Double FIRMWARE_VERSION;

    private Double OPERATING_TIME;

    private Double REMAINING_BATTERY_LIFE_TIME;

    public Double getFIRMWARE_VERSION() {
        return FIRMWARE_VERSION;
    }

    public void setFIRMWARE_VERSION(Double FIRMWARE_VERSION) {
        this.FIRMWARE_VERSION = FIRMWARE_VERSION;
    }

    public Double getOPERATING_TIME() {
        return OPERATING_TIME;
    }

    public void setOPERATING_TIME(Double OPERATING_TIME) {
        this.OPERATING_TIME = OPERATING_TIME;
    }

    public Double getREMAINING_BATTERY_LIFE_TIME() {
        return REMAINING_BATTERY_LIFE_TIME;
    }

    public void setREMAINING_BATTERY_LIFE_TIME(Double REMAINING_BATTERY_LIFE_TIME) {
        this.REMAINING_BATTERY_LIFE_TIME = REMAINING_BATTERY_LIFE_TIME;
    }
}
