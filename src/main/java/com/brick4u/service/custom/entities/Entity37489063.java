package com.brick4u.service.custom.entities;

import org.influxdb.annotation.Column;
import org.influxdb.annotation.Measurement;

@Measurement(name="MDT-A511-37489063-07")
public class Entity37489063 {
    @Column(name="time")
    private String time;

    @Column(name="FLOW_TEMPERATURE")
    private Double FLOW_TEMPERATURE;

    @Column(name="NOT_SUPPORTED")
    private Integer NOT_SUPPORTED;

    @Column(name="OPERATING_TIME")
    private Integer OPERATING_TIME;

    @Column(name="REMAINING_BATTERY_LIFE_TIME")
    private  Integer REMAINING_BATTERY_LIFE_TIME;

    @Column(name="VOLUME")
    private Double VOLUME;

    @Column(name="VOLUME_FLOW")
    private Double VOLUME_FLOW;

    @Column(name="VOLUME_FLOW_EXT")
    private Double VOLUME_FLOW_EXT;

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public Double getFLOW_TEMPERATURE() {
        return FLOW_TEMPERATURE;
    }

    public void setFLOW_TEMPERATURE(Double FLOW_TEMPERATURE) {
        this.FLOW_TEMPERATURE = FLOW_TEMPERATURE;
    }

    public Integer getNOT_SUPPORTED() {
        return NOT_SUPPORTED;
    }

    public void setNOT_SUPPORTED(Integer NOT_SUPPORTED) {
        this.NOT_SUPPORTED = NOT_SUPPORTED;
    }

    public Integer getOPERATING_TIME() {
        return OPERATING_TIME;
    }

    public void setOPERATING_TIME(Integer OPERATING_TIME) {
        this.OPERATING_TIME = OPERATING_TIME;
    }

    public Integer getREMAINING_BATTERY_LIFE_TIME() {
        return REMAINING_BATTERY_LIFE_TIME;
    }

    public void setREMAINING_BATTERY_LIFE_TIME(Integer REMAINING_BATTERY_LIFE_TIME) {
        this.REMAINING_BATTERY_LIFE_TIME = REMAINING_BATTERY_LIFE_TIME;
    }

    public Double getVOLUME() {
        return VOLUME;
    }

    public void setVOLUME(Double VOLUME) {
        this.VOLUME = VOLUME;
    }

    public Double getVOLUME_FLOW() {
        return VOLUME_FLOW;
    }

    public void setVOLUME_FLOW(Double VOLUME_FLOW) {
        this.VOLUME_FLOW = VOLUME_FLOW;
    }

    public Double getVOLUME_FLOW_EXT() {
        return VOLUME_FLOW_EXT;
    }

    public void setVOLUME_FLOW_EXT(Double VOLUME_FLOW_EXT) {
        this.VOLUME_FLOW_EXT = VOLUME_FLOW_EXT;
    }
}
