package com.brick4u.service.custom.entities;

import org.influxdb.annotation.Column;
import org.influxdb.annotation.Measurement;

@Measurement(name="MDT-7916-45980661-37")
public class Entity45980661 {
    @Column(name = "time")
    private String time;

    @Column(name = "ENERGY")
    private  Double	ENERGY;

    @Column(name = "POWER")
    private Double POWER;

    @Column(name = "POWER_2")
    private Double    POWER_2;

    @Column(name = "POWER_3")
    private Double    POWER_3;

    @Column(name = "POWER_4")
    private Double POWER_4;

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public Double getENERGY() {
        return ENERGY;
    }

    public void setENERGY(Double ENERGY) {
        this.ENERGY = ENERGY;
    }

    public Double getPOWER() {
        return POWER;
    }

    public void setPOWER(Double POWER) {
        this.POWER = POWER;
    }

    public Double getPOWER_2() {
        return POWER_2;
    }

    public void setPOWER_2(Double POWER_2) {
        this.POWER_2 = POWER_2;
    }

    public Double getPOWER_3() {
        return POWER_3;
    }

    public void setPOWER_3(Double POWER_3) {
        this.POWER_3 = POWER_3;
    }

    public Double getPOWER_4() {
        return POWER_4;
    }

    public void setPOWER_4(Double POWER_4) {
        this.POWER_4 = POWER_4;
    }
}
